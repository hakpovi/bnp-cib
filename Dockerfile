
FROM openjdk:11-jre-slim

RUN apt-get update && apt-get install -y postgresql-client

COPY . .


CMD ["java", "-jar", "*.jar"]

CMD ["sleep", "infinity"]
